#
# INPUT:
# ARGV[0] values_filename: fichero que contiene todos los eventos en formato patient_id \t event_name \t event_date
# ARGV[1] patients_filename: fichero con lista de pacientes patient_id \t sex \t birthdate
# ARGV[2] covars_filename: fichero con la lista de covariables, duración, nombre corto y largo, etc
#

require 'date'

#values_filename = '../all_data_final.txt'
#patients_filename = '../patients_final.txt'
#covars_filename = '../covars/names/COVARS_FULL.txt'


values_filename = ARGV[0]
patients_filename = ARGV[1]
covars_filename = ARGV[2]
output_filename = ARGV[3]

covars_dur = {}
covars = []
File.foreach(covars_filename) { |x|
  a = x.split("\t")
  unless a[0]=="SHORTNAME"
    covars_dur[a[0]] = a[2].to_i
    covars.push(a[0].to_s)
  end
}


patients = {}
File.foreach(patients_filename) { |x|
  a = x.split("\t")
  unless a[0] == 'id'
    patients[a[0].to_i] = {sex: a[1], yearnac: Date.strptime(a[2],"%d/%m/%Y").year, covars: Array.new(covars.length, 0)}
  end

}

puts "Patients created"
puts patients.keys.length


File.foreach(values_filename) { |x|



  a = x.split("\t")
  raw_name = a[1]


  if raw_name.index('_FE')
    var_name = raw_name[0..raw_name.index('_FE') - 1].gsub(/[^0-9a-z]/i, '')
    if (Date.strptime("01/06/2011",'%d/%m/%Y').jd - Date.strptime(a[2],'%d/%m/%Y').jd) <= covars_dur[var_name].to_i
      patients[a[0].to_i][:covars][covars.index(var_name)] = 1
    end

  end
}
puts "Done"

head_str = "id\tsex\tyearnac"
covars.each do |covar_name|
  head_str = head_str + "\t" + covar_name
end
head_str = head_str + "\r\n"

File.open(output_filename, 'w') {|f| f.write(head_str)}

table_str = ""
counter = 0
patients.keys.each do |k|
  new_row = k.to_s + "\t" + patients[k][:sex].to_s + "\t" + patients[k][:yearnac].to_s
  covars.each do |covar_key|
    new_row = new_row + "\t" + patients[k][:covars][covars.index(covar_key)].to_s
  end
  table_str= table_str + new_row + "\r\n"
  if counter % 500 == 0
    puts counter
    File.open(output_filename, 'a') {|f| f.write(table_str)}
    table_str = ""
    new_row = ""
  end
  counter = counter + 1
end
File.open(output_filename, 'a') {|f| f.write(table_str)}
puts counter
