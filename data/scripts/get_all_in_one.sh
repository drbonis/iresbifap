#!/bin/bash
cat "$1"/* > concat.txt
cat concat.txt | grep -v patient_id > concat2.txt
head -n 1 concat.txt > header.txt
cat header.txt concat2.txt > output.txt
rm concat.txt
rm concat2.txt
rm header.txt
