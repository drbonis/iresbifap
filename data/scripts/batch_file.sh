#!/bin/bash
# $1 folder que contiene los datos brutos originales

echo "Inicio"
mkdir "$1"/processed
mkdir "$1"/processed/all
mkdir "$1"/processed/covars
mkdir "$1"/processed/covars/names
mkdir "$1"/processed/covars/values
for f in "$1"/*.txt
do
  echo "Procesando $filename"
  echo "Recodificando a UTF-8: recode -f UTF-8 $f"
  recode -f UTF-8 $f
  filename=$(basename $f)
  echo "Extrayendo covariables de $filename"
  ruby datacooking_paso_4.rb $f "$1"/processed/covars/names/${filename%.*}_COVARS.txt "$1"/processed/all/PATIENTS.txt "$1"/processed/covars/values
  echo "Finalizado: $filename"
done

cat "$1"/processed/covars/names/* > "$1"/processed/all/all_names.txt

cat "$1"/processed/covars/values/* > concat.txt
cat concat.txt | grep -v patient_id > concat2.txt
head -n 1 concat.txt > header.txt
cat header.txt concat2.txt > "$1"/processed/all/all_values.txt
rm concat.txt
rm concat2.txt
rm header.txt
