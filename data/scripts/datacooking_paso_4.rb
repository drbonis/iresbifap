#
# a partir de un fichero de resultados del generador de variables
# genera una lista de ficheros cada uno con el nombre de una variables
# con el formato patient_id \t covar_name \t date
# asi como un fichero que contiene la lista de los nombres de las variables
#
# nota: para recodificar a UTF-8 usar antes en terminal 'recode -f UTF-8 <filename>'
#

input_filename = ARGV[0]
list_of_names_filename = ARGV[1]
patients_list_filename = ARGV[2]
values_target_dir = ARGV[3]




cols_head = File.open(input_filename, &:readline).split("\t");


selected_cols = [0,2,3] # las columnas de id, sexo y fecha de nacimiento las incluyo siempre
selected_cols_names = ['id','sex','nac']
covar = {}
covar_count = {}


if(ARGV[2])
  patients_list_output = "id\tsex\tnac\r\n"
  File.open(patients_list_filename, 'w') {|f| f.write(patients_list_output)}
  patients_list_output = ""
end

#aqui selecciono las variables a extraer
c = 0
cols_head.each do |col|
  if col.include? "_FE"
    covar_name = col[0..col.index('_FE') - 1].gsub(/[^0-9a-z]/i, '')
    selected_cols.push(c)
    selected_cols_names.push(covar_name)
    covar[covar_name] = "patient_id\tcovar_name\tdate\r\n"
    covar_count[covar_name] = 0
  end
  c = c + 1
end



covar_name_output = ""
covar.keys.each do |covar_name|

  covar_name_output = covar_name_output + covar_name + "\r\n"
  File.open(values_target_dir + "/BF_" + covar_name + ".txt", 'w') {|f| f.write("")}
end
File.open(list_of_names_filename, 'w') {|f| f.write(covar_name_output)}



i = 0
File.foreach(input_filename) { |x|
  if i > 0
    cols = x.split("\t")
    if(ARGV[2])
      patients_list_output = patients_list_output + cols[0] + "\t" + cols[2] + "\t" + cols[3] + "\r\n"
    end
    selected_cols.each do |index|
      if index > 3 && !cols[index].include?("99/99/9999")
        covar_name = cols_head[index][0..cols_head[index].index('_FE') - 1].gsub(/[^0-9a-z]/i, '')
        if cols[index].strip != ""
          covar[covar_name] = covar[covar_name]  + cols[0] + "\t" + covar_name + "\t" + cols[index].strip + "\r\n"
        else
          covar[covar_name] = covar[covar_name]  + cols[0] + "\t" + covar_name + "\t01/01/1800\r\n"
        end
        covar_count[covar_name] = covar_count[covar_name] + 1
        if covar_count[covar_name]%1000 == 0
          File.open(values_target_dir + "/BF_" + covar_name + ".txt", 'a') {|f| f.write(covar[covar_name])}
          covar[covar_name] = ""
          covar_count[covar_name] = 0
        end
      end
    end
  end
  if i%1000 == 0
    if(ARGV[2])
      File.open(patients_list_filename, 'a') {|f| f.write(patients_list_output)}
      patients_list_output = ""
    end
  end
  i = i + 1
}
covar_count.keys.each do |covar_name|
  File.open(values_target_dir + "/BF_" + covar_name + ".txt", 'a') {|f| f.write(covar[covar_name])}
end

if(ARGV[2])
  File.open(patients_list_filename, 'a') {|f| f.write(patients_list_output)}
end
