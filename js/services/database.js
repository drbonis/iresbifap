(function(){

    angular.module("bifapApp")

    .factory("Database", function DatabaseFactory($rootScope){
        var self = this;

        indexedDB.deleteDatabase("bifapDB"); // al inicio borra base de datos si existe

        self.schemaBuilder = lf.schema.create('bifapDB', 1); // crea el schemaBuilder
        bfdebug = self;
        // funciones de Database

        self.filterColumn = function(db,table,column,value) {
          return new Promise(function(resolve,reject){
              var tableSchema = db.getSchema().table(table);
              db.select().from(tableSchema).where(tableSchema[column].eq(value)).exec().then(function(rows){
                resolve(rows);
              });
          });
        };

        self.selectPatients = function(params) {
          /*
          params.agemin 0
          params.agemax 110
          params.sex []
          params.event_id
          params.duration_of_event
          */
          //console.log(params);
          //console.log(params.covar_id);
          var params = params || {}
          params.sex = params.sex || [0,1];

          if(params.sex.length > 1) {
            sexmin = 0;
            sexmax = 1;
          } else {
            if(params.sex[0] == 0) {
              sexmin = 0;
              sexmax = 0;
            } else if (params.sex[0]) {
              sexmin = 1;
              sexmax = 1
            } else {
              sexmin = 0;
              sexmax = 1;
            }
          }
          var params = {
            'agemin': params.agemin || 0,
            'agemax': params.agemax || 110,
            'sexmin': sexmin,
            'sexmax': sexmax,
            'covar_id': params.covar_id || [],
            'duration_of_event': parseInt(params.duration_of_event) || 36500,
            'refer_date': params.refer_date || $rootScope.timeFormat.parse("01/06/2011")
          }

          return new Promise(function(resolve,reject){
              var db = self.bifapDB;
              if(db) {
                var patient = db.getSchema().table('Patient');
                var evento = db.getSchema().table('Event');
                var covariable = db.getSchema().table('Covariable');

                var checkDates = function(rows) {
                  var results = [];
                  rows.forEach(function(row){
                    var a = $rootScope.timeFormat.parse("01/06/2011").getTime() - row.Event.date.getTime();
                    var timeSinceEvent = parseInt(a / (24*60*60*1000));
                    if (timeSinceEvent <= params.duration_of_event + 365) {
                      results.push(row);
                    }
                  });

                  return results;
                }

                if(params.covar_id.length == 1) {

                  db.select(patient.id, patient.yearnac, patient.sex, covariable.shortname, evento.date).from(evento,patient,covariable)
                    .where(
                      lf.op.and(
                        evento.patient_id.eq(patient.id),
                        evento.covariable_id.eq(covariable.id),
                        patient.sex.between(params['sexmin'],params['sexmax']),
                        patient.yearnac.between(2011-(params['agemax']-1) ,2011-params['agemin']),
                        covariable.id.eq(params['covar_id'][0])
                      )
                    )
                    .exec().then(function(rows){
                        resolve(checkDates(rows));
                  });
                } else if (params.covar_id.length == 0) {
                  db.select(patient.id, patient.yearnac, patient.sex).from(patient)
                    .where(
                      lf.op.and(
                        patient.sex.between(params['sexmin'],params['sexmax']),
                        patient.yearnac.between(2011-(params['agemax']-1)  ,2011-params['agemin'])
                      )
                    )
                    .exec().then(function(rows){
                        resolve(rows);
                  });
                }
              } else {
                  resolve(false);
              }
          });
        };

        self.insertValues = function(tableName,rowList,db) {
          return new Promise(function(resolve,reject){
            var rowListToInsert = [];
            var tableSchema = db.getSchema().table(tableName);
            rowList.forEach(function(row){
              rowListToInsert.push(tableSchema.createRow(row));
            });
            db.insert().into(tableSchema).values(rowListToInsert).exec().then(function(r){
              resolve(r);
            });
          });
        };

        var Database =
         {
            seedBifapInit: function() {
              return new Promise(function(resolve,reject){
                var patientTableBuilder = self.schemaBuilder.createTable('Patient');
                var covariableTableBuilder = self.schemaBuilder.createTable('Covariable');
                var eventTableBuilder = self.schemaBuilder.createTable('Event');

                patientTableBuilder.addColumn('id', lf.Type.INTEGER).
                    addColumn('sex', lf.Type.INTEGER).
                    addColumn('yearnac', lf.Type.INTEGER).
                    addPrimaryKey(['id']).
                    addIndex('idxSex', ['sex'], false, lf.Order.ASC).
                    addIndex('idxYearnac', ['yearnac'], false, lf.Order.ASC);

                covariableTableBuilder.addColumn('id', lf.Type.INTEGER).
                    addColumn('shortname', lf.Type.STRING).
                    addColumn('fullname', lf.Type.STRING).
                    addColumn('defaultdur', lf.Type.INTEGER).
                    addColumn('type', lf.Type.STRING).
                    addColumn('description', lf.Type.STRING).
                    addColumn('keywords', lf.Type.STRING).
                    addPrimaryKey(['id']).
                    addIndex('idxShortname', ['shortname'], false, lf.Order.ASC).
                    addIndex('idxFullname', ['fullname'], false, lf.Order.ASC).
                    addIndex('idxType', ['type'], false, lf.Order.ASC).
                    addIndex('idxKeywords', ['keywords'], false, lf.Order.ASC);

                eventTableBuilder.addColumn('id', lf.Type.INTEGER).
                    addColumn('patient_id', lf.Type.INTEGER).
                    addColumn('covariable_id', lf.Type.STRING).
                    addColumn('date', lf.Type.DATE_TIME).
                    addPrimaryKey(['id'], true).
                    addIndex('idxPatientId', ['patient_id'], false, lf.Order.ASC).
                    addIndex('idxCovariableId', ['covariable_id'], false, lf.Order.ASC).
                    addIndex('idxValue', ['date'], false, lf.Order.ASC);
                resolve(true);
              });
            },

            seedCovariable: function(db,covariable_id,covarEventsFilename) {
              return new Promise(function(resolve,reject){
                self.filterColumn(db, 'Event', 'covariable_id', covariable_id).then(function(eventList){
                  if(eventList.length == 0) {
                    //covariable nunca ha sido cargada
                    //cargar eventos
                    //console.log("nunca ha sido cargada");
                      d3.tsv(covarEventsFilename + '?foo='+eval(new Date().getTime()), function(eventsdata){
                        eventRows = [];
                        eventsdata.forEach(function(eventInfo){
                          newEvent = {
                            'patient_id': eventInfo.patient_id,
                            'covariable_id': covariable_id,
                            'date': $rootScope.timeFormat.parse(eventInfo.date)
                          }
                          eventRows.push(newEvent);
                        });
                        //console.log(eventRows);
                        self.insertValues('Event', eventRows, db).then(function(r){
                          //console.log("incluido en base de datos");
                          resolve(r);
                        });

                      });
                  } else {
                    resolve(true);
                  }
                });
              });
            },

            getTable: function(tablename,db) {
              return new Promise(function(resolve,reject){
                  var tableSchema = db.getSchema().table(tablename);
                  resolve(tableSchema);
              });
            },

            connect: function(){
                return new Promise(function(resolve,reject){
                  self.schemaBuilder.connect().then(function(db){
                    self.bifapDB = db;
                    resolve(self.bifapDB);
                  });
                });
            },
            bifapDB: function(){
              return self.bifapDB;
            },

            insertValues: self.insertValues,

            filterColumn: self.filterColumn,

            selectPatients: self.selectPatients
        }

        return Database;

    });
})()
