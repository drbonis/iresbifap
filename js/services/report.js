(function(){

    angular.module("bifapApp")

    .factory("Report", function ReportFactory(){
        var self = this;
        self.elements = [];
        self.autoCount = 1;

        self.elementsByVariable = function(variable_id) {
          results = [];
          self.elements.forEach(function(elem){
            if (elem.variable_ids.indexOf(variable_id)>-1) {
              results.push(elem.uid);
            }
          });
          return results;
        },

        self.elementExists = function(type,variable_ids) {
          // if an element with same type an same variable_ids exists in the report, returns index, else -1.
          var exists = -1;
          self.elements.forEach(function(elem,i){

            if(elem.type == type) {
              var checkVars = true;
              variable_ids.forEach(function(var_id){
                if(elem.variable_ids.indexOf(var_id) < 0) {
                  checkVars = false;
                }
              });
              if(checkVars) { exists = i};
            }
          });
          return exists;
        }

        self.deleteElementsByVariable = function(variable_id) {
          var elementIdList = self.elementsByVariable(variable_id);
          elementIdList.forEach(function(element_id){
            self.deleteElement(element_id);
          });
          return true;
        }

        self.deleteElement = function(uid) {
          // do not delete element from array but just set visible to false
          self.elements.forEach(function(elem){
            if(elem.uid == uid) {
              elem.visible = false;
            }
          })
        }

        var Report =
         {
            autoCount: function() {
                return self.autoCount;
            },

            all: function() {
                return self.elements;
            },

            addElement: function(newElement) {
              var newElementIndex = self.elementExists(newElement.type, newElement.variable_ids);
              if(newElementIndex>-1){ // the newElement already exists in Report.elements
                self.elements[newElementIndex].visible = true;
              } else {
                newElement.visible = true;
                newElement.uid = self.autoCount + 1;
                self.elements.push(newElement);
                self.autoCount = self.autoCount + 1;
              };

            },

            deleteElement: self.deleteElement,
            elementsByVariable: self.elementsByVariable,
            deleteElementsByVariable: self.deleteElementsByVariable,
            elementExists: self.elementExists,

            deleteAllContingencyTables: function() {
              self.elements.forEach(function(elem){
                if(elem.type == 'contingency-table') {
                  elem.visible = false;
                }
              })
            },

            moveElement: function(currPos,newPos) {
              console.log(self.elements);
            }
        }
        return Report;
    })
})()
