(function(){
    angular.module("bifapApp")

    .factory("Variable", function VariableFactory(Database){
        var self = this;
        self.variables = {};
        self.autoCount = 1;


        var Variable =
         {
            autoCount: function() {
                return self.autoCount;
            },
            byId: function(id) {
                return self.variables[id];
            },
            catalogue: function() {
              return self.variables;
            },
            add: function(newVariable) {
              newVariable.SHORTNAME = newVariable.SHORTNAME || '';
              newVariable.FULLNAME = newVariable.FULLNAME || '';
              newVariable.TYPE = newVariable.TYPE || '';
              newVariable.KEYWORDS = newVariable.KEYWORDS || '';
              newVariable.DESCRIPTION = newVariable.DESCRIPTION || '';
              newVariable.DEFAULTDUR = newVariable.DEFAULTDUR || 0;


              newVariable.id = self.autoCount;
              newVariable.selected = newVariable.selected || false;
              newVariable.filtered = newVariable.filtered || false;
              newVariable.loading = false;
              newVariable.CONTINGENCYTABLES = {};
              self.autoCount = self.autoCount + 1;
              self.variables[newVariable.id] = newVariable;
              return self.variables[newVariable.id];
            },
            selected: function() {
                    var varlist = [];
                    Object.keys(self.variables).forEach(function(key){
                        if(self.variables[key].selected) {
                           varlist.push(self.variables[key]);
                        }
                    });
                    return varlist;
            },
            filterByTxt: function(txt) {
                             Object.keys(self.variables).forEach(function(key){
                                 txt = txt.toUpperCase();
                                 if(self.variables[key].tinyname.toUpperCase().indexOf(txt) > -1
                                     || self.variables[key].shortname.toUpperCase().indexOf(txt) > -1
                                     || self.variables[key].fullname.toUpperCase().indexOf(txt) > -1
                                     || self.variables[key].keywords.toUpperCase().indexOf(txt) > -1
                                     ) {
                                         self.variables[key].filtered = true;
                                     } else {
                                         self.variables[key].filtered = false;
                                     }
                             });
                             return true;
           },
            unfilterAll: function() {
                            Object.keys(self.variables).forEach(function(key){
                              self.variables[key].filtered = false;
                            });
            },

            addContingencyTable: function(contingencyTableData) {
              self.variables[contingencyTableData.variableA.id].CONTINGENCYTABLES[contingencyTableData.variableB.id] = contingencyTableData;
              self.variables[contingencyTableData.variableB.id].CONTINGENCYTABLES[contingencyTableData.variableA.id] = contingencyTableData;
            },

            getContingencyTable: function(variableA_id,variableB_id) {
              if(
                Object.keys(self.variables[variableA_id]).indexOf(variableA_id.toString()) > -1 &&
                Object.keys(self.variables[variableA_id].CONTINGENCYTABLES).indexOf(variableB_id.toString())>-1
                )
              {
                return self.variables[variableA_id].CONTINGENCYTABLES[variableB_id];
              } else {
                // no ha sido calculada nunca devuelvo false
                return false;
              }
            }
        }
        return Variable;
    })
})()
