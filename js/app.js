(function(){


    angular.module('bifapApp',['ngAnimate'])

    .directive('bfCover', function(){
        return {
            restricti: 'E',
            templateUrl: "./templates/bf-cover.html?foo=" + eval(new Date().getTime())
        }
    })

    .directive('bfVariableCard', function(){
        return {
            restrict: 'E',
            templateUrl: "./templates/variable/bf-variable-card.html?foo=" + eval(new Date().getTime())
        }
    })

    .directive('bfVariableCardSm', function(){
        return {
            restrict: 'E',
            templateUrl: "./templates/variable/bf-variable-sm.html?foo=" + eval(new Date().getTime())
        }
    })


    .directive('bfLayoutMyVariables', function(){
        return {
            restrict: 'E',
            templateUrl: "./templates/layout/bf-my-variables.html?foo=" + eval(new Date().getTime())
        }
    })

    .directive('bfLayoutVariablesCatalogue', function(){
        return {
            restrict: 'E',
            templateUrl: "./templates/layout/bf-variables-catalogue.html?foo=" + eval(new Date().getTime())
        }
    })

    .directive('bfLayoutReport', function(){
        return {
            restrict: 'E',
            templateUrl: "./templates/layout/bf-report.html?foo=" + eval(new Date().getTime())
        }
    })


    .directive('bfInputSearchVariable', ['Variable', function(Variable){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs){
                elem.bind("input change", function(){
                  if(elem.val().length == 0) {
                    Variable.unfilterAll();
                    scope.tab.showFullCatalogue = false;
                    scope.tab.showATCLevel = "";
                    scope.tab.inputSearchIsNull = true;
                  } else {
                    scope.tab.showFullCatalogue = false;
                    scope.tab.showATCLevel = "";
                    scope.tab.inputSearchIsNull = false;
                    if(elem.val().length > 2) {
                        Variable.filterByTxt(elem.val());
                    }
                  }
                  scope.$apply();
                });
            }
        }
    }])

    .directive('bfCategory', function(){
      return {
        restrict: 'A',
        link: function(scope,elem, attrs) {
          elem.bind("click", function(){
               var categoryLabel = elem.html();
               scope.tab.showATCLevel = categoryLabel;
               // nota: esta parte está duplicada en controlador data,
               // habria que sacarlo al servicio Variable de alguna forma???
               $("#inputCatalogueSearch").val(categoryLabel);
               $("#inputCatalogueSearch").trigger("change");
           });
        }
      }
    })

    .directive('bfGraph', function(){
      // add a bar graph
      return {
        restrict: 'E',
        scope: {
          data: '='
        },
        template: '<div class="myGraph"></div>',
        link: function(scope,element,attrs) {
          element.children().attr("id","chart-"+scope.data.uid.toString());
          var x = []; var maley = []; var femaley = [];
          scope.data.data.forEach(function(ageRangeData){
            x.push(ageRangeData.agerange[0].toString() + " a " + ageRangeData.agerange[1].toString() + " años");
            maley.push(ageRangeData.male);
            femaley.push(ageRangeData.female);
          });

          var trace1 = {
            x: x,
            y: maley,
            name: 'Hombres',
            type: 'bar'
          };

          var trace2 = {
            x: x,
            y: femaley,
            name: 'Mujeres',
            type: 'bar'
          };

          var data = [trace1, trace2];

          var layout = {
            barmode: 'group'
          };

          var options = {
            displayModeBar: false
          }

          Plotly.newPlot('chart-'+scope.data.uid.toString(), data, layout, options);

        }
      }
    });
})();
