(function(){
	angular.module('bifapApp')
	
	.controller("TabController",function(Database, Variable, $scope,$rootScope){
			var self = this;
			self.isDatasetLoaded = false;
			self.isVarsetSelected = false;
			self.layout = 0;
			self.isGroupHelpHover = false;
			self.isReportHelpHover = false;
			self.helpReportUnderstood = false;
			self.helpGroupUnderstood = false;
			self.isGroupBeingEdited = false;
			self.loading = false;
			self.loadingvariable = false;
			self.loadingProgress = 0;
			self.variableSelectedInReportId = "0";
			self.variableSelectedInReport = {};
			self.inputSearchIsNull = true;
			self.showFullCatalogue = false; // shows the full only text catalogue of categories
			self.showATCLevel = ""




			$scope.$on('groupEditing', function(event, groupVar){
					self.isGroupBeingEdited = groupVar.selectedToEditAsGroup;
			});


			$scope.$on('datasetLoading', function(event,data){
					self.loading = true;
					self.loadingProgress = data.loadingProgress;
					$("#loading-progress").html(self.loadingProgress+"%");
					$("#loading-progress-bar").attr('aria-valuenow',self.loadingProgress);
					$("#loading-progress-bar").attr('style',"width: "+ self.loadingProgress + "%;");

			});

			$scope.$on('datasetLoaded', function(event){
					self.isDatasetLoaded = true;
					self.loading = false;
					self.loadingProgress = 100;
					$("#loading-progress").html(self.loadingProgress+"%");
					$scope.$apply()

			});



			this.toggleCategories = function() {
				Variable.unfilterAll();
				this.inputSearchIsNull = true;
				this.showFullCatalogue = !this.showFullCatalogue;
				$("#inputCatalogueSearch").val("");
			}




			this.helpReportUnderstand = function() {
					this.helpReportUnderstood = true;
			}

			this.helpGroupUnderstand = function() {
					this.helpGroupUnderstood = true;
			}

			this.toggleHoverGroupIcon = function() {
					this.isGroupHelpHover = !this.isGroupHelpHover;
			}

			this.toggleHoverReportIcon = function() {
					this.isReportHelpHover = !this.isReportHelpHover;
			}

			this.setLayoutNumber = function(layout) {
					this.layout = layout;
			}
			this.setLayout = function(sourceBar){

					if(sourceBar === 'catalogue') {
							switch(this.layout) {
									case 0:

											this.layout = 1;
											break;
									case 1:

											this.layout = 0;
											break;
									case 2:

											this.layout = 1;
											break;
							}
					}
					if(sourceBar === 'report') {
							switch(this.layout) {
									case 0:

											this.layout = 2;
											break;
									case 1:

											this.layout = 2;
											break;
									case 2:

											this.layout = 0;
											break;
							}
					}

			}

			this.setSection = function(section,chapter){
					this.selSection = section;
					this.hideSubmenu[chapter] = false;
			}
			this.setChapter = function(chapter){
					this.selChapter = chapter;
					this.hideSubmenu[chapter] = this.hideSubmenu[chapter]? false : true;
			}
			this.isSection = function(section){
					return eval(section === this.selSection);
			}
			this.isChapter = function(chapter){
					return eval(chapter === this.selChapter);
			}
			this.setVariableSelectedInReport = function(id){
				self.variableSelectedInReportId = id;
				if(Database.bifapDB()){
					Database.filterColumn(Database.bifapDB(),'Covariable','id',id).then(function(r){
						self.variableSelectedInReport = r[0];
						$scope.$apply();
					});

				} else {
					return [];
				};
			}
	})


})()
