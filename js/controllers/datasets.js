(function(){

angular.module('bifapApp')

.controller("DatasetController", function(Report,Variable,Database,$scope,$rootScope,$timeout){
		var self = this;
		//var datasource_path = './data/development';
		var datasource_path = './data/production';
		//var datasource_path = './data/test';
		$rootScope.timeFormat = d3.time.format("%d/%m/%Y");

		self.setup = {
				intervalDays: 365,
				refDate: $rootScope.timeFormat.parse("01/06/2011")
		}
		// Seeding functions

		self.seedBIFAP = function($event) {
			//seeds the list of patients at Patient table and
			//the reference table of variables Variable

			$rootScope.$broadcast('datasetLoading',{'loadingProgress': 0});
			Database.seedBifapInit().then(function(){
				d3.tsv(datasource_path+'/PATIENTS.txt?foo='+eval(new Date().getTime()), function(patientsdata){

					d3.tsv(datasource_path+'/COVARS_FULL.txt?foo='+eval(new Date().getTime()), function(covarsdefdata){

						var varnameList = []; var patientList = []; var patientNacEventRows = [];
						Database.connect().then(function(db){

							Database.getTable('Patient',db).then(function(r){

								var patient_table = r; var mycount = 1;

								patientsdata.forEach(function(newPatient){
										patientList.push({
											'id': newPatient.id,
											'sex': newPatient.sex,
											'yearnac': new Date($rootScope.timeFormat.parse(newPatient.nac)).getFullYear()
											});

									if(mycount % 2000 == 0) {
										var localcount = mycount;
										Database.insertValues('Patient',patientList,db).then(function(r){
											var loadProgress = Math.round((((localcount / 2)/ patientsdata.length))*100)+50 ;
											$rootScope.$broadcast('datasetLoading',{'loadingProgress': loadProgress});
										});
										patientList = [];
									}

									mycount = mycount + 1;
								});

								Database.insertValues('Patient', patientList, db).then(function(r){

									var covarsList = [];
									covarsdefdata.forEach(function(covardef){
										var newCovar = {
											'tinyname': covardef.SHORTNAME || '',
											'shortname': covardef.SHORTNAME || '',
											'fullname': covardef.FULLNAME || '',
											'type': covardef.TYPE || '',
											'keywords': covardef.KEYWORDS || '',
											'description': covardef.DESCRIPTION || '',
											'defaultdur': covardef.DEFAULTDUR || 0
										}
										var r = Variable.add(newCovar);
										covarsList.push(newCovar);
									});
									Database.insertValues('Covariable', covarsList, db).then(function(r){
										$rootScope.$broadcast('datasetLoaded');
									});
								});
							});
						});
					});
				}).on("progress",function(event){
					if(d3.event.lengthComputable) {
						 var loadProgress = Math.round((((d3.event.loaded / 2) / d3.event.total))*100);
						 $rootScope.$broadcast('datasetLoading',{'loadingProgress': loadProgress});
					}
				});
			});
		}


		//Report functions
		self.addGeneralElement = function (elementName,variableId) {
			return new Promise(function(resolve,reject){
				self.createGeneralElements[elementName](variableId).then(function(){
					resolve(true);
				});
			});
		}

		self.addMultivariateTable = function(varsArray,dependentVarPos){
			return new Promise(function(resolve,reject){
				self.createGeneralElements['varMultivariateTable'](varsArray,dependentVarPos).then(function(){
					resolve(true);
				})
			});
		}
		self.getReportElements = function() {
				//console.log(Report.all());
				return Report.all();
		}
		self.removeReportElement = function(element) {
			Report.deleteElement(element.uid);
		}
		self.removeReportElementByVariable = function(variable_id) {
			Report.deleteElementByVariable(variable_id);
		}

		self.createGeneralElements = {

				'varPyramidTable': function(varId) {
						 return new Promise(function(resolve,reject){
							 if(Report.elementExists('table',[varId])>-1){
								 var varPyramidTable = {
										 'variable_ids': [varId],
										 'type': 'table'
									}
								 Report.addElement(varPyramidTable);
								 $scope.$apply();
								 resolve(true); // table is already in Report.elements so no calculation needed
							 } else {
								 var variable = Variable.byId(varId);
								 switch(variable.type) {
									 case "FARMACO":
										 table_title = 'Prevalencia (%) de uso de ' + variable.fullname + ' por edad y sexo';
										 break;
									 case "DIAGNOSTIC":
										 table_title = 'Prevalencia (%) de casos de ' + variable.fullname + ' por edad y sexo';
										 break;
									 default:
										 table_title = 'Prevalencia (%) de ' + variable.fullname + ' por edad y sexo';
									}
								self.getBasicStatsFromCases(variable).then(function(datatable){
									// consider use bf-matrix.js BFMatrix object here in the future
									var varPyramidTable = {
											'uid': null, // is set by Report.addElement...
											'variable_ids': [variable.id],
											'type': 'table',
											'title': table_title,
											'cells': [
													["Grupo de edad", "% Mujeres (n)", "% Hombres (n)", "% Ambos sexos (n)"]
												],
											 'foot': "Distribución por edad y sexo a partir de una muestra aleatoria de 50.000 pacientes de BIFAP."
									};
									for(j=0;j<datatable.data.row_titles.length;j++){
										var datarow = datatable.showrow(datatable.data.row_titles[j]);
										var newRow = [];
										newRow.push(datatable.data.row_titles[j]);
										for(i=0;i<datarow.length;i++) {
											var percentage = null;
											if(datarow[i].patients.length==0) {
												percentage = eval(0).toFixed(1);
											} else {
												percentage = (100 * (datarow[i].cases.length / datarow[i].patients.length)).toFixed(1);
											}
											newRow.push(percentage.toString() + " % (" + datarow[i].cases.length.toString() + ")");
										}
										varPyramidTable.cells.push(newRow);
									}
									Report.addElement(varPyramidTable);
									$scope.$apply();
									resolve(true);
								});
							 };

					 }); // Promise ends
				},

				'varPyramidGraph': function(varId) {
					return new Promise(function(resolve,reject){

						if(Report.elementExists('graph',[varId])>-1){
							var varPyramidGraph = {
									'variable_ids': [varId],
									'type': 'graph'
							}
							Report.addElement(varPyramidGraph);
							$scope.$apply();
							resolve(true);
						} else {
							var variable = Variable.byId(varId);
							self.getBasicStatsFromCases(variable).then(function(datatable){
								var graph_title = "";
								switch(variable.type) {
									case "FARMACO":
										graph_title = 'Prevalencia (%) de uso de ' + variable.fullname + ' por edad y sexo';
										break;
									case "DIAGNOSTIC":
										graph_title = 'Prevalencia (%) de casos de ' + variable.fullname + ' por edad y sexo';
										break;
									default:
										graph_title = 'Prevalencia (%) de ' + variable.fullname + ' por edad y sexo';
								}
								var varPyramidGraph = {
										'uid': null, // is set by Report.addElement...
										'variable_ids': [variable.id],
										'type': 'graph',
										'title': graph_title,
										'data': [],
										'foot': "Distribución por edad y sexo a partir de una muestra aleatoria de 50.000 pacientes de BIFAP."

								};
								for(i=0;i<datatable.data.row_titles.length;i++) {
									var thisRow = datatable.showrow(datatable.data.row_titles[i]);
									var agerange = [thisRow[0].agemin, thisRow[0].agemax];
									var pmujeres = thisRow[0].cases.length / thisRow[0].patients.length;
									var phombres = thisRow[1].cases.length / thisRow[1].patients.length;

									if(thisRow[0].patients.length == 0) {phombres = 0};
									if(thisRow[1].patients.length == 0) {pmujeres = 0};
									varPyramidGraph.data.push({'agerange': agerange, 'male': phombres, 'female': pmujeres});
								}
								Report.addElement(varPyramidGraph);
								$scope.$apply();
								resolve(true);
							});
						};




					});
				},

				'varContingencyTable': function(cTableData) {
					return new Promise(function(resolve,reject){
						var varContingencyTable = {
								'uid': null, // is set by Report.addElement...
								'variable_ids': [cTableData.variableA.id, cTableData.variableB.id],
								'type': 'contingency-table',
								'p_value': cTableData.evidencia,
								'title': "Asociación entre " + cTableData.variableA.fullname + " y " + cTableData.variableB.fullname,
								'cells': [
										["", cTableData.variableA.shortname + " SI", cTableData.variableA.shortname + " NO", "Total"],
										[cTableData.variableB.shortname + " SI", cTableData.table.Cell_A.toString(), cTableData.table.Cell_B.toString(), eval(cTableData.table.Cell_A + cTableData.table.Cell_B).toString()],
										[cTableData.variableB.shortname + " NO", cTableData.table.Cell_C.toString(), cTableData.table.Cell_D.toString(), eval(cTableData.table.Cell_C + cTableData.table.Cell_D).toString()],
										["Total", eval(cTableData.table.Cell_A + cTableData.table.Cell_C).toString(), eval(cTableData.table.Cell_B + cTableData.table.Cell_D).toString(), eval(cTableData.table.Cell_A + cTableData.table.Cell_B + cTableData.table.Cell_C + cTableData.table.Cell_D).toString()]
									],
								'foot': cTableData.mensaje
						};
						Report.addElement(varContingencyTable);
						$scope.$apply();
						resolve(true);
					});
				},

				'varMultivariateTable': function(varsArray,dependentVarPos){
					return new Promise(function(resolve,reject){
						var varMultivariateTable = {
							'uid': null,
							'variable_ids': [],
							'type': 'multivariable-table',
							'title': "Tabla multivariante",
							'dependent_var_pos': 0,
							'head': 'Cabecera de la tabla',
							'cells': [
								['','Diabetes SI', 'Diabetes NO','OR[IC95%]','p-value'],
								['% Metformina','50%','50%','1 [0.8,1-2]','0.05'],
								['% Insulina','50%','50%','1 [0.8,1-2]','0.05'],
								['% Sitagliptina','50%','50%','1 [0.8,1-2]','0.05']
							],
							'foot': 'Pide de la página'
						};
						Report.addElement(varMultivariateTable);
						$scope.$apply();
						resolve(true);
					});
				}
		};


		//Variables functions
		self.variableFilterByTxt = function(text) {
			$("#inputCatalogueSearch").val(text);
			$("#inputCatalogueSearch").trigger("change");
		}
		self.variableById = function(id) {
			if(Database.bifapDB()){
				Database.filterColumn(Database.bifapDB(),'Covariable','id',id).then(function(r){
					return r;
				});

			} else {
				return [];
			};
		}


		self.getCatalogueVariables = function(sortByAttr) {
				var catalogue = Variable.catalogue();
				var catalogueArray = [];
				Object.keys(catalogue).forEach(function(key){
					if(catalogue[key].filtered) {
						catalogueArray.push(catalogue[key]);
					}
				});
				catalogueArray.sort(function(a,b){
					if (a.shortname > b.shortname) return 1;
					if (a.shortname < b.shortname) return -1;
					return 0;
				});
				return catalogueArray;
		}
		self.getSelectedVariables = function() {
				var a = Variable.selected();
				return a;
		}

		self.summarySelector = function(){
				var diseaseVars = [];
				var drugVars = [];
				self.getSelectedVariables().forEach(function(v){
					switch(v.type){
						case 'DIAGNOSTIC':
							diseaseVars.push(v);
							break;
						case 'FARMACO':
							drugVars.push(v);
							break;
						default:
							break;
					}
				});
				return {'diseases': diseaseVars, 'drugs': drugVars};
		}

		self.toggleSelected = function(variable) {
			//se ejecuta al pulsar la estrella en el catalogo de variables
				if(variable.selected==false) { // variable se añade
					variable.loading = true;
					db = Database.bifapDB();
					Database.filterColumn(db,'Covariable','shortname',variable.shortname).then(function(covariableInfo){
						Database.seedCovariable(db,covariableInfo[0].id,datasource_path+"/covars/values/BF_" + variable.tinyname + ".txt").then(function(r){
							variable.loading = false;
							variable.selected = true;
							self.computeContingencyTables().then(function(contingencyTables){
								self.addGeneralElement('varPyramidGraph',variable.id).then(function(){
										self.addGeneralElement('varPyramidTable',variable.id).then(function(){
											contingencyTables.forEach(function(cTable){
													self.addGeneralElement('varContingencyTable',cTable);
											});
											self.addGeneralElement('varMultivariateTable',[],0);
										});
								});
							});
							// here the logic that decides the summary tables depending
							// on number of variables and type of tableSchema
							console.log(self.summarySelector());
							$rootScope.$apply();
							return r;
						});
					});
				} else { // variable se elimina
					variable.selected = false;
					Report.deleteElementsByVariable(variable.id); // del all elements of report associated to that variable
					if(self.getSelectedVariables().length == 0) {
						// code executed when last variable removed so there are no variables
					}
				}
		}

		self.computeContingencyTables = function() {
			// generates all contingency tables of variables selected
			// first lets hidde all previous Report.elements of type contingnecy table
			Report.deleteAllContingencyTables();
			// now generate all contingency tables
			var variablesArray = self.getSelectedVariables();
			var promisesArray = [];
			var contingencyTablesAll = [];
			if(variablesArray.length > 1) {
				for(i=0;i<variablesArray.length;i++){
					for(j=i+1;j<variablesArray.length;j++){
						promisesArray.push(self.getContingencyTable(variablesArray[i],variablesArray[j]).then(function(r){
							contingencyTablesAll.push(r);
						}));
					}
				}
			}
			return new Promise(function(resolve,reject){
				Promise.all(promisesArray).then(results => {
					resolve(contingencyTablesAll);
				});
			});
		}

		self.getContingencyTable = function(variableA, variableB) {
			var precalculatedContingencyTable = Variable.getContingencyTable(variableA.id,variableB.id);
			if(precalculatedContingencyTable) {
				// if r false then contingency table is precalculated and stored in Variable.variables and is not
				// need to query data base nor calculate contingency table
				var r = false;
				var promisesArray = [];
			} else {
							var r = {'ri': [], 'rj': [], 'rt': [], 'variable_ri': variableA, 'variable_rj': variableB}
							var queryA = Database.selectPatients(
								 {
									 'covar_id': [variableA.id],
									 'sex': [0,1],
									 'agemin': 0,
									 'agemax': 109,
									 'duration_of_event': parseInt(variableA.defaultdur)
								 }
							 ).then(function(ri){
								 r.ri = ri;
							 });

							 var queryB = Database.selectPatients(
							 {
								 'covar_id': [variableB.id],
								 'sex': [0,1],
								 'agemin': 0,
								 'agemax': 109,
								 'duration_of_event': parseInt(variableB.defaultdur)
							 }).then(function(rj){
								 r.rj = rj;
							 });

							 var queryT = Database.selectPatients(
							 {
								 'covar_id': [],
								 'sex': [0,1],
								 'agemin': 0,
								 'agemax': 109,
							 }).then(function(rt){
								 r.rt = rt;
							 })
							var promisesArray = [queryA,queryB,queryT];
			}

			return new Promise(function(resolve,reject){
				Promise.all(promisesArray).then(results => {
					//r is false in case of precalculated contingency table
					//else is a {} with data.
					var finalContingencyTable = null;
					if(r) {

						function Fmt(x,dec) {
							var dec = dec || 2;
							var v;
							if(x>=0) { v=''+(x+0.0005) } else { v=''+(x-0.0005) };
							return v.substring(0,v.indexOf('.')+dec+1);
						}

						var ri_patients = [];
						var rj_patients = [];
						r.ri.forEach(function(ri_el){
							ri_patients.push(parseInt(ri_el.Patient.id));
						});
						r.rj.forEach(function(rj_el){
							rj_patients.push(parseInt(rj_el.Patient.id));
						});

						var ri_rj_count = 0;
						var rin_rj_count = 0;
						var ri_rjn_count = 0;
						var rin_rjn_count = 0;
						ri_patients.forEach(function(ri_el){
							if(rj_patients.indexOf(ri_el)>-1) {
								ri_rj_count = ri_rj_count + 1;
							} else {
								ri_rjn_count = ri_rjn_count + 1
							}
						});
						rj_patients.forEach(function(rj_el){
							if(ri_patients.indexOf(rj_el)==-1) {
								rin_rj_count = rin_rj_count + 1
							}
						});


						rin_rjn_count = r.rt.length - ri_rj_count - rin_rj_count - ri_rjn_count;

						var zScores = {'0.05': 1.97, '0.01': 2.57, '0.001': 3.29}
						var evidencia_text = {
							'0.001': "Existe una evidencia muy fuerte (p<0.001)",
							'0.01': "Existe una evidencia fuerte (p<0.01)",
							'0.05': "Existe una evidencia moderada (p<0.05)",
							'>=0.05': "No existe evidencia (p>=0.05)"
						};

						var form = {'Cell_A': ri_rj_count, 'Cell_B': rin_rj_count, 'Cell_C': ri_rjn_count, 'Cell_D': rin_rjn_count};
						var or = (form.Cell_A * form.Cell_D) / (form.Cell_B * form.Cell_C);

						var standardError = Math.sqrt((1/form.Cell_A)+(1/form.Cell_B)+(1/form.Cell_C)+(1/form.Cell_D));
						var ef = {	'0.05': Math.exp(zScores['0.05']*standardError),
										 '0.01': Math.exp(zScores['0.01']*standardError),
											 '0.001': Math.exp(zScores['0.001']*standardError)
										 }
						 var or_l = {	'0.05': or / ef['0.05'],
													 '0.01': or / ef['0.01'],
													 '0.001': or / ef['0.001']
												 }
						 var or_h = {	'0.05': or * ef['0.05'],
													 '0.01': or * ef['0.01'],
													 '0.001': or * ef['0.001']
												 }

						 var evidencia = null;
						 var or_label = null;

						 if(or_l['0.001'] > 1 || or_h['0.001'] < 1) {
							 evidencia = '0.001';
						 } else if (or_l['0.01'] > 1 || or_h['0.01'] < 1) {
							 evidencia = '0.01';
						 } else if (or_l['0.05'] > 1 || or_h['0.05'] < 1) {
							 evidencia = '0.05';
						 } else {
							 evidencia = '>=0.05';
						 };

						 or_label = "OR: " + Fmt(or) + " [ " + Fmt(or_l['0.05']) + " - " + Fmt(or_h['0.05']) + " ] 95%CI";

						 var mensaje = null; var sentido_asociacion = null; var intensidad = null;
						 if(evidencia!='>=0.05') {
							 if(or < 1) {
								 sentido_asociacion = "negativa"
							 } else if (or > 1){
								 sentido_asociacion = "positiva"
							 };
							 if(or > 0.80 && or < 1.20) {
								 intensidad = 'intensidad debil'
							 } else if((or > 0.5 && or <= 0.80) || (or >= 1.20 && or < 2 )) {
								 intensidad = 'intensidad moderada'
							 } else if((or > 0.25 && or <= 0.5) || (or >= 2 && or < 4 )) {
								 intensidad = 'intensidad fuerte'
							 } else {
								 intensidad = 'intensidad muy fuerte'
							 }
							 mensaje = evidencia_text[evidencia] + " de asociación " + sentido_asociacion + " de " + intensidad + " (" + or_label + ") entre " + r.variable_ri.fullname + " y " + r.variable_rj.fullname;
						 } else {
							 mensaje = evidencia_text[evidencia] + " de asociación entre " + r.variable_ri.fullname + " y " + r.variable_rj.fullname + ". " + or_label;
						 };

						 var finalContingencyTable = {
							 'variableUIDs': [variableA.id, variableB.id],
							 'variableA': variableA,
							 'variableB': variableB,
							 'table': form,
							 'or': or,
							 'or_l': or_l,
							 'or_h': or_h,
							 'or_label': or_label,
							 'standardError': standardError,
							 'ef': ef,
							 'mensaje': mensaje,
							 'sentido_asociacion': sentido_asociacion,
							 'intensidad': intensidad,
							 'evidencia': evidencia
						 }
						 Variable.addContingencyTable(finalContingencyTable);
					} else {
						//precalculated contingency table
						finalContingencyTable = Variable.getContingencyTable(variableA.id,variableB.id);
					}
					resolve(finalContingencyTable);
				});
			});
		}

		self.getBasicStatsFromCases = function(variable,params) {

				var eventDuration = parseInt(variable.defaultdur) || 3650000;
				var params = params || {};
				params.ageGroups = params.ageGroups || [0,10,20,30,40,50,60,70,80,110];
				params.sexGroups = params.sexGroups || [[0],[1],[0,1]];
				params.ageMin = params.ageMin || 0;
				params.ageMax = params.ageMax || 109;

				m = new BFMatrix();
				m.agesextable(params.ageGroups,params.sexGroups);

				var promisesArray = [];

				m.data.cells.forEach(function(cell,index){
					var p1 = Database.selectPatients(
						{
							'covar_id': [variable.id],
							'sex': cell.sex,
							'agemin': cell.agemin,
							'agemax': cell.agemax,
							'duration_of_event': [eventDuration]
						}
					).then(function(r){
						m.data.cells[index].cases = r;
					});
					var p2 = Database.selectPatients(
						{
							'covar_id': [],
							'sex': cell.sex,
							'agemin': cell.agemin,
							'agemax': cell.agemax
						}
					).then(function(r_patients){
						m.data.cells[index].patients = r_patients;
					});
					promisesArray.push(p1,p2);
				});
				return new Promise(function(resolve,reject){
					Promise.all(promisesArray).then(results => {
						resolve(m);
					});
				});
		}
})

})()
