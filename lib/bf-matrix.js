/*
Genera una funcion que contiene una matriz
con diversas funciones para mostrar las filas, columnas, celdas
de dicha matriz.
*/

function BFMatrix() {
	this.data = {
		'row_titles': [],
		'col_titles': [],
		'cells': []
	}
};

BFMatrix.prototype.blank = function(row_titles,col_titles) {
	this.data.row_titles = row_titles;
	this.data.col_titles = col_titles;
	for(i=0;i<this.data.row_titles.length;i++) {
		for(j=0;j<this.data.col_titles.length;j++) {
			this.data.cells.push(this.data.row_titles[i]+this.data.col_titles[j]);
		}
	}
};

BFMatrix.prototype.showallcells = function() {
	return(this.data);
}


BFMatrix.prototype.showcell = function(row,col) {
	var i = this.data.row_titles.indexOf(row);
	var j = this.data.col_titles.indexOf(col);
	var numcols = this.data.col_titles.length;
	if(i > -1 && j > -1) {
		return this.data.cells[((numcols*i)) + j];
	} else { return false }
}

BFMatrix.prototype.showcellindex = function(row,col) {
	var i = this.data.row_titles.indexOf(row);
	var j = this.data.col_titles.indexOf(col);
	var numcols = this.data.col_titles.length;
	if(i > -1 && j > -1) {
		return ((numcols*i)) + j;
	} else { return false }
}

BFMatrix.prototype.showcol = function(col) {
	var j = this.data.col_titles.indexOf(col);
	if(j > -1) {
		var numcols = this.data.col_titles.length;
		var pointer = j;
		var results = [];
		while(pointer < this.data.cells.length) {
			results.push(this.data.cells[pointer]);
			pointer = pointer + numcols;
		}
	} else {
		results = false;
	}
	return results;
}

BFMatrix.prototype.showrow = function(row) {
	var i = this.data.row_titles.indexOf(row);
	if(i > -1) {
		var numcols = this.data.col_titles.length;
		var pointer = i*numcols;
		var results = [];
		while(pointer < (i+1)*numcols) {
			results.push(this.data.cells[pointer]);
			pointer = pointer + 1;
		}
	} else {
		results = false;
	}
	return results;
}


BFMatrix.prototype.agesextable = function(ages,sexes) {
	for(i=0;i<ages.length-1;i++) {
		this.data.row_titles.push(ages[i].toString() + " a " + ages[i+1].toString());
		for(j=0;j<sexes.length;j++) {
			var sextag = '';
			if(sexes[j].length > 1) {
				sextag = 'Ambos';
			} else {
				if(sexes[j][0] == 1) {
					sextag = 'Hombre';
				} else {
					sextag = 'Mujer';
				}
			}
			if (this.data.col_titles.indexOf(sextag)<0) {
				this.data.col_titles.push(sextag)
			} ;
			this.data.cells.push(
				{
					'agemin': ages[i],
					'agemax': ages[i+1],
					'sex': sexes[j],
					'cases': null,
					'patients': null
				}
			);
		}
	}
}
