


	function Ln(x) { return Math.log(x) } function Exp(x) { return Math.exp(x)}
	function xlx(x) { return x*Ln(x+1e-20) }
	function Abs(x) { return Math.abs(x) }
	function Sqrt(x) { return Math.sqrt(x) }
	function Cos(x) { return Math.cos(x) }
	function Pow(x,n) { return Math.pow(x,n) }
	function fEnt( x ) { return x * Ln(x) / Ln(2) }
	var Pi=3.141592653589793; Pi2=2*Pi; LnPi2 = Ln(Pi2); PiD2=Pi/2
	var Cell_A; var Cell_B; var Cell_C; var Cell_D
	var Cell_r1; var Cell_r2; var Cell_c1; var Cell_c2; var t
	var Ex_A; var Ex_B; var Ex_C; var Ex_D; var Sav_A; var Sav_B; var Sav_C; var Sav_D
	var cs; var od; var rr; var kp; var fc; var mcr; var sn; var sp; var pp; var np
	var arr; var rrr; var plr; var nlr; var dor; var yj; var nnd
	var dp; var nn; var nmi; var cc; var ca; var cp; var yq; var ets

	function CalcTots(form) {
		// needs form = {'Cell_A': int, 'Cell_B': int, 'Cell_C': int, 'Cell_D' = int}
		Cell_A = eval(form.Cell_A)
		Cell_B = eval(form.Cell_B)
		Cell_C = eval(form.Cell_C)
		Cell_D = eval(form.Cell_D)
		Cell_r1 = Cell_A+Cell_B
		Cell_r2 = Cell_C+Cell_D
		Cell_c1 = Cell_A+Cell_C
		Cell_c2 = Cell_B+Cell_D
		t = Cell_A+Cell_B+Cell_C+Cell_D
		form.Cell_r1 = ''+(Cell_r1)
		form.Cell_r2 = ''+(Cell_r2)
		form.Cell_c1 = ''+(Cell_c1)
		form.Cell_c2 = ''+(Cell_c2)
		form.t = ''+(t)
		return form;
	}

	function CalcStats(form) {
		var LoSlop = Cell_A; if(Cell_D<Cell_A) { LoSlop = Cell_D }
		var HiSlop = Cell_B; if(Cell_C<Cell_B) { HiSlop = Cell_C }
		var LnProb1 = LnFact(Cell_r1) + LnFact(Cell_r2) + LnFact(Cell_c1) + LnFact(Cell_c2) - LnFact(t)
		var SingleP = Exp( LnProb1 - LnFact(Cell_A) - LnFact(Cell_B) - LnFact(Cell_C) - LnFact(Cell_D) )
		var FisherP=0; var LeftP=0; var RightP=0; var RosnerP=0; var SumCheck=0
		var k = Cell_A - LoSlop
		while( k<=Cell_A+HiSlop ) {
			var P = Exp( LnProb1 - LnFact(k) - LnFact(Cell_r1-k) - LnFact(Cell_c1-k) - LnFact(k+Cell_r2-Cell_c1) )
			SumCheck = SumCheck + P
			if( k<=Cell_A ) { LeftP = LeftP + P }
			if( k>=Cell_A ) { RightP = RightP + P }
			if( P<=(SingleP+1e-12) ) { FisherP = FisherP + P }
			k = k + 1
		}
		form.LeftP = Fmt(LeftP)
		form.RightP = Fmt(RightP)
		form.FisherP = Fmt(FisherP)
		form.SingleP = Fmt(SingleP)
		form.SumCheck = "" + SumCheck
		RosnerP = 0.5
		if( LeftP<RosnerP ) { RosnerP = LeftP }
		if( RightP<RosnerP ) { RosnerP = RightP }
		RosnerP = 2*RosnerP
		form.RosnerP = Fmt(RosnerP)
		Ex_A = Cell_r1*Cell_c1/t; Sav_A = Ex_A
		Ex_B = Cell_r1*Cell_c2/t; Sav_B = Ex_B
		Ex_C = Cell_r2*Cell_c1/t; Sav_C = Ex_C
		Ex_D = Cell_r2*Cell_c2/t; Sav_D = Ex_D
		cs=csq(Cell_A,Ex_A,.5)+csq(Cell_B,Ex_B,.5)+csq(Cell_C,Ex_C,.5)+csq(Cell_D,Ex_D,.5)
		form.csyc = Fmt(cs)
		form.csyc_p = Fmt(Csp(cs))
		od=(Cell_A/Cell_B)/(Cell_C/Cell_D); form.od=Fmt(od);


		rr=(Cell_A/Cell_r1)/(Cell_C/Cell_r2); form.rr=Fmt(rr)
		kp=2*(Cell_A*Cell_D-Cell_B*Cell_C)/((Cell_B+Cell_A)*(Cell_B+Cell_D)+(Cell_A+Cell_C)*(Cell_D+Cell_C)); form.kp=Fmt(kp)
		fc=(Cell_A+Cell_D)/t; form.fc=Fmt(fc); form.mcr=Fmt(1-fc)
		sn=Cell_A/Cell_c1; form.sn=Fmt(sn)
		sp=Cell_D/Cell_c2; form.sp=Fmt(sp)
		pp=Cell_A/Cell_r1; form.pp=Fmt(pp)
		np=Cell_D/Cell_r2; form.np=Fmt(np)
		plr=sn/(1-sp); form.plr=Fmt(plr)
		nlr=(1-sn)/sp; form.nlr=Fmt(nlr)
		dor=(sn/(1-sn))/((1-sp)/sp); form.dor=Fmt(dor)
		eor=(sn/(1-sn))/(sp/(1-sp)); form.eor=Fmt(eor)
		yj=sn+sp-1; form.yj=Fmt(yj)
		nnd=1/yj; form.nnd=Fmt(nnd)
		nnm=1/(1-fc); form.nnm=Fmt(nnm)
		dp=Cell_A/Cell_r1-Cell_C/Cell_r2; form.dp=Fmt(dp)
		arr=-dp; form.arr=Fmt(arr)
		rrr=arr/(Cell_C/Cell_r2); form.rrr=Fmt(rrr)
		nmi = 1 - ( xlx(Cell_B+Cell_A) + xlx(Cell_D+Cell_C) - xlx(Cell_B) - xlx(Cell_A) - xlx(Cell_D) - xlx(Cell_C) ) / ( xlx(t) - xlx(Cell_c2) - xlx(Cell_c1) ); form.nmi=Fmt(nmi)
		csny=csq(Cell_B,Ex_B,0)+csq(Cell_A,Ex_A,0)+csq(Cell_D,Ex_D,0)+csq(Cell_C,Ex_C,0)
		form.csny = Fmt(csny)
		form.csny_p = Fmt(Csp(csny))
		csmh=(t-1)*(Cell_A*Cell_D-Cell_B*Cell_C)*(Cell_A*Cell_D-Cell_B*Cell_C)/(Cell_r1*Cell_r2*Cell_c1*Cell_c2)
		form.csmh = Fmt(csmh)
		form.csmh_p = Fmt(Csp(csmh))
		cc=Sqrt(csny/(csny+t)); form.cc=Fmt(cc)
		ca=cc*Sqrt(2); form.ca=Fmt(ca)
		rtet=Cos(Pi/(1+Sqrt(od))); form.rtet = Fmt(rtet)
		cp=(Cell_A*Cell_D-Cell_B*Cell_C)/Sqrt(Cell_r1*Cell_r2*Cell_c2*Cell_c1); form.cp=Fmt(cp)
		yq=(Cell_A*Cell_D-Cell_B*Cell_C)/(Cell_B*Cell_C+Cell_A*Cell_D); form.yq=Fmt(yq)
		ets = (Cell_A - Sav_A) / (Cell_A + Cell_B + Cell_C - Sav_A); form.ets=Fmt(ets)
		EntR = - ( fEnt(Cell_r1/t) + fEnt(Cell_r2/t) ); form.EntR=Fmt(EntR)
		EntC = - ( fEnt(Cell_c1/t) + fEnt(Cell_c2/t) ); form.EntC=Fmt(EntC)
		EntRC = - ( fEnt(Cell_A/t) + fEnt(Cell_B/t) + fEnt(Cell_C/t) + fEnt(Cell_D/t) ); form.EntRC=Fmt(EntRC)
		EntIB = EntR + EntC - EntRC; form.EntIB=Fmt(EntIB)
		EntIA = EntRC - EntR; form.EntIA=Fmt(EntIA)
		EntIC = EntRC - EntC; form.EntIC=Fmt(EntIC)
		EntSim = EntIB / ( EntIA + EntIB + EntIC ); form.EntSim=Fmt(EntSim)
		EntDif = 1 - EntSim; form.EntDif=Fmt(EntDif)
		var pcrit = (100-form.ConfLevel)/100
		form.Conf_Int_Caption = form.ConfLevel+"% Conf. Interval"
		var del=LoSlop
		Ex_B=Cell_B+LoSlop; Ex_A=Cell_A-LoSlop; Ex_D=Cell_D-LoSlop; Ex_C=Cell_C+LoSlop; var pval=0
		while(del>0.000001) {
			del=del/2
			if(pval<pcrit) { Ex_B=Ex_B-del } else { Ex_B=Ex_B+del }
			Ex_A=Cell_r1-Ex_B; Ex_D=Cell_c2-Ex_B; Ex_C=Cell_r2-Ex_D
			pval=Csp(csq(Cell_B,Ex_B,0.5)+csq(Cell_A,Ex_A,0.5)+csq(Cell_D,Ex_D,0.5)+csq(Cell_C,Ex_C,0.5))
		}
		form.Low_A=Fmt(Ex_A); form.Low_B=Fmt(Ex_B); form.Low_C=Fmt(Ex_C); form.Low_D=Fmt(Ex_D);
		od=(Ex_A/Ex_B)/(Ex_C/Ex_D); form.od_lo=Fmt(od);

		rr=(Ex_A/Cell_r1)/(Ex_C/Cell_r2); form.rr_lo=Fmt(rr)
		kp=2*(Ex_A*Ex_D-Ex_B*Ex_C)/((Ex_B+Ex_A)*(Ex_B+Ex_D)+(Ex_A+Ex_C)*(Ex_D+Ex_C)); form.kp_lo=Fmt(kp)
		fc=(Ex_A+Ex_D)/(Ex_B+Ex_A+Ex_D+Ex_C); form.fc_lo=Fmt(fc); form.mcr_hi=Fmt(1-fc)
		sn=Ex_A/Cell_c1; form.sn_lo=Fmt(sn)
		sp=Ex_D/Cell_c2; form.sp_lo=Fmt(sp)
		plr=sn/(1-sp); form.plr_lo=Fmt(plr)
		nlr=(1-sn)/sp; form.nlr_hi=Fmt(nlr)
		dor=(sn/(1-sn))/((1-sp)/sp); form.dor_lo=Fmt(dor)
		eor=(sn/(1-sn))/(sp/(1-sp)); form.eor_lo=Fmt(eor)
		yj=sn+sp-1; form.yj_lo=Fmt(yj)
		nnd=1/yj; form.nnd_hi=Fmt(nnd)
		nnm=1/(1-fc); form.nnm_lo=Fmt(nnm)
		pp=Ex_A/Cell_r1; form.pp_lo=Fmt(pp)
		np=Ex_D/Cell_r2; form.np_lo=Fmt(np)
		dplo=Ex_A/Cell_r1-Ex_C/Cell_r2; form.dp_lo=Fmt(dplo)
		arr=-dplo; form.arr_hi=Fmt(arr)
		rrr=arr/(Ex_C/Cell_r2); form.rrr_hi=Fmt(rrr)
		nmi = 1 - ( xlx(Ex_B+Ex_A) + xlx(Ex_D+Ex_C) - xlx(Ex_B) - xlx(Ex_A) - xlx(Ex_D) - xlx(Ex_C) ) / ( xlx(t) - xlx(Cell_c2) - xlx(Cell_c1) ); form.nmi_lo=Fmt(nmi)
		csny=csq(Ex_B,Sav_B,0)+csq(Ex_A,Sav_A,0)+csq(Ex_D,Sav_D,0)+csq(Ex_C,Sav_C,0)
		cc=Sqrt(csny/(csny+t)); form.cc_lo=Fmt(cc)
		ca=cc*Sqrt(2); form.ca_lo=Fmt(ca)
		rtet=Cos(Pi/(1+Sqrt(od))); form.rtet_lo = Fmt(rtet)
		cp=(Ex_A*Ex_D-Ex_B*Ex_C)/Sqrt(Cell_r1*Cell_r2*Cell_c2*Cell_c1); form.cp_lo=Fmt(cp)
		yq=(Ex_A*Ex_D-Ex_B*Ex_C)/(Ex_B*Ex_C+Ex_A*Ex_D); form.yq_lo=Fmt(yq)
		ets = (Ex_A - Sav_A) / (Ex_A + Ex_B + Ex_C - Sav_A); form.ets_lo=Fmt(ets)
		EntR = - ( fEnt(Cell_r1/t) + fEnt(Cell_r2/t) ); form.EntR_lo=Fmt(EntR)
		EntC = - ( fEnt(Cell_c1/t) + fEnt(Cell_c2/t) ); form.EntC_lo=Fmt(EntC)
		EntRC = - ( fEnt(Ex_A/t) + fEnt(Ex_B/t) + fEnt(Ex_C/t) + fEnt(Ex_D/t) ); form.EntRC_lo=Fmt(EntRC)
		EntIB = EntR + EntC - EntRC; form.EntIB_hi=Fmt(EntIB)
		EntIA = EntRC - EntR; form.EntIA_lo=Fmt(EntIA)
		EntIC = EntRC - EntC; form.EntIC_lo=Fmt(EntIC)
		EntSim = EntIB / ( EntIA + EntIB + EntIC ); form.EntSim_hi=Fmt(EntSim)
		EntDif = 1 - EntSim; form.EntDif_lo=Fmt(EntDif)
		del=HiSlop
		Ex_B=Cell_B-HiSlop; Ex_A=Cell_A+HiSlop; Ex_D=Cell_D+HiSlop; Ex_C=Cell_C-HiSlop; var pval=0
		while(del>0.000001) {
		del=del/2
		if(pval<pcrit) { Ex_B=Ex_B+del } else { Ex_B=Ex_B-del }
		Ex_A=Cell_r1-Ex_B; Ex_D=Cell_c2-Ex_B; Ex_C=Cell_r2-Ex_D
		pval=Csp(csq(Cell_B,Ex_B,0.5)+csq(Cell_A,Ex_A,0.5)+csq(Cell_D,Ex_D,0.5)+csq(Cell_C,Ex_C,0.5))
		}
		form.High_A=Fmt(Ex_A); form.High_B=Fmt(Ex_B); form.High_C=Fmt(Ex_C); form.High_D=Fmt(Ex_D);
		od=(Ex_A/Ex_B)/(Ex_C/Ex_D);
		form.od_hi=Fmt(od);

		rr=(Ex_A/Cell_r1)/(Ex_C/Cell_r2); form.rr_hi=Fmt(rr)
		kp=2*(Ex_A*Ex_D-Ex_B*Ex_C)/((Ex_B+Ex_A)*(Ex_B+Ex_D)+(Ex_A+Ex_C)*(Ex_D+Ex_C)); form.kp_hi=Fmt(kp)
		fc=(Ex_A+Ex_D)/(Ex_B+Ex_A+Ex_D+Ex_C); form.fc_hi=Fmt(fc); form.mcr_lo=Fmt(1-fc)
		sn=Ex_A/Cell_c1; form.sn_hi=Fmt(sn)
		sp=Ex_D/Cell_c2; form.sp_hi=Fmt(sp)
		plr=sn/(1-sp); form.plr_hi=Fmt(plr)
		nlr=(1-sn)/sp; form.nlr_lo=Fmt(nlr)
		dor=(sn/(1-sn))/((1-sp)/sp); form.dor_hi=Fmt(dor)
		eor=(sn/(1-sn))/(sp/(1-sp)); form.eor_hi=Fmt(eor)
		yj=sn+sp-1; form.yj_hi=Fmt(yj)
		nnd=1/yj; form.nnd_lo=Fmt(nnd)
		nnm=1/(1-fc); form.nnm_hi=Fmt(nnm)
		pp=Ex_A/Cell_r1; form.pp_hi=Fmt(pp)
		np=Ex_D/Cell_r2; form.np_hi=Fmt(np)
		dphi=Ex_A/Cell_r1-Ex_C/Cell_r2; form.dp_hi=Fmt(dphi)
		arr=-dphi; form.arr_lo=Fmt(arr)
		rrr=arr/(Ex_C/Cell_r2); form.rrr_lo=Fmt(rrr)
		nmi = 1 - ( xlx(Ex_B+Ex_A) + xlx(Ex_D+Ex_C) - xlx(Ex_B) - xlx(Ex_A) - xlx(Ex_D) - xlx(Ex_C) ) / ( xlx(t) - xlx(Cell_c2) - xlx(Cell_c1) ); form.nmi_hi=Fmt(nmi)
		csny=csq(Ex_B,Sav_B,0)+csq(Ex_A,Sav_A,0)+csq(Ex_D,Sav_D,0)+csq(Ex_C,Sav_C,0)
		cc=Sqrt(csny/(csny+t)); form.cc_hi=Fmt(cc)
		ca=cc*Sqrt(2); form.ca_hi=Fmt(ca)
		rtet=Cos(Pi/(1+Sqrt(od))); form.rtet_hi = Fmt(rtet)
		cp=(Ex_A*Ex_D-Ex_B*Ex_C)/Sqrt(Cell_r1*Cell_r2*Cell_c2*Cell_c1); form.cp_hi=Fmt(cp)
		yq=(Ex_A*Ex_D-Ex_B*Ex_C)/(Ex_B*Ex_C+Ex_A*Ex_D); form.yq_hi=Fmt(yq)
		ets = (Ex_A - Sav_A) / (Ex_A + Ex_B + Ex_C - Sav_A); form.ets_hi=Fmt(ets)
		EntR = - ( fEnt(Cell_r1/t) + fEnt(Cell_r2/t) ); form.EntR_hi=Fmt(EntR)
		EntC = - ( fEnt(Cell_c1/t) + fEnt(Cell_c2/t) ); form.EntC_hi=Fmt(EntC)
		EntRC = - ( fEnt(Ex_A/t) + fEnt(Ex_B/t) + fEnt(Ex_C/t) + fEnt(Ex_D/t) ); form.EntRC_hi=Fmt(EntRC)
		EntIB = EntR + EntC - EntRC; form.EntIB_lo=Fmt(EntIB)
		EntIA = EntRC - EntR; form.EntIA_hi=Fmt(EntIA)
		EntIC = EntRC - EntC; form.EntIC_hi=Fmt(EntIC)
		EntSim = EntIB / ( EntIA + EntIB + EntIC ); form.EntSim_lo=Fmt(EntSim)
		EntDif = 1 - EntSim; form.EntDif_hi=Fmt(EntDif)
		if(dp==0) { form.nn="Infinite" } else { form.nn=Fmt(Math.abs(1/dp)) }
		form.nn_lo="Unknown"; form.nn_hi="Unknown"
		if(dplo<0 & dphi<0) { form.nn_lo=Fmt(-1/dplo); form.nn_hi=Fmt(-1/dphi) }
		if(dplo<0 & dphi==0) { form.nn_Lo=Fmt(-1/dplo); form.nn_hi="Infinite" }
		if(dplo<0 & dphi>0) { form.nn_lo=Fmt(1/Math.max(Math.abs(dplo),Math.abs(dphi))); form.nn_hi="Infinite" }
		if(dplo==0 & dphi>0) { form.nn_lo=Fmt(1/dphi); form.nn_hi="Infinite" }
		if(dplo>0 & dphi>0) { form.nn_lo=Fmt(1/dphi); form.nn_hi=Fmt(1/dplo) }
		return form;
	}
	function csq(o,e,y) {
	if(e==0) { return 0 }
	var x=Abs(o-e)-y; if(x<0) { return 0 }
	return x*x/e
	}
	function Csp(x) {
	return ChiSq(x,1)
	}
	function ChiSq(x,n) {
		if(x>1000 | n>1000) { var q=Norm((Pow(x/n,1/3)+2/(9*n)-1)/Sqrt(2/(9*n)))/2; if (x>n) {return q}{return 1-q} }
		var p=Exp(-0.5*x); if((n%2)==1) { p=p*Sqrt(2*x/Pi) }
		var k=n; while(k>=2) { p=p*x/k; k=k-2 }
		var t=p; var Cell_B=n; while(t>1e-15*p) { Cell_B=Cell_B+2; t=t*x/Cell_B; p=p+t }
		return 1-p
	}

	function Norm(z) { var q=z*z
		if(Abs(z)>7) {return (1-1/q+3/(q*q))*Exp(-q/2)/(Abs(z)*Sqrt(PiD2))} {return ChiSq(q,1) }
	}
	function Fmt(x) { var v
		if(x>=0) { v=''+(x+0.0005) } else { v=''+(x-0.0005) }
		return v.substring(0,v.indexOf('.')+4)
	}
	function LnFact(z) {
	if(z<2) { return 0 }
	if(z<17) { f=z; while(z>2) { z=z-1; f=f*z }; return Ln(f) }
	return (z+0.5)*Ln(z) - z + LnPi2/2 + 1/(12*z) - 1/(360*Pow(z,3)) + 1/(1260*Pow(z,5)) - 1/(1680*Pow(z,7))
	}
